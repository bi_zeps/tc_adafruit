- [AdafruitFeatherM0](#adafruit-feather-m0)

#  Adafruit Feather M0

### 0.19.0-r1
* Changed to Ubuntu 20.04

### 0.19.0-r0
* Initial version
* Alpine 3.14.2
* arduino-cli 0.19.0