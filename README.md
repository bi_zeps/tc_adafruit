[![Pipeline Master](https://img.shields.io/gitlab/pipeline/bi_zeps/tc_adafruit/master?label=master&logo=gitlab)](https://gitlab.com/bi_zeps/tc_adafruit)
[![License](https://img.shields.io/badge/dynamic/json?color=orange&label=license&query=%24.license.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F18136148%3Flicense%3Dtrue)](https://gitlab.com/bi_zeps/toolchains/-/blob/master/LICENSE)
[![Open Issues](https://img.shields.io/badge/dynamic/json?color=yellow&logo=gitlab&label=open%20issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F18136148%2Fissues_statistics)](https://gitlab.com/bi_zeps/tc_adafruit/-/issues)
[![Last Commit](https://img.shields.io/badge/dynamic/json?color=green&logo=gitlab&label=last%20commit&query=%24[:1].committed_date&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F18136148%2Frepository%2Fcommits%3Fbranch%3Dmaster)](https://gitlab.com/bi_zeps/tc_adafruit/-/commits/master)

[![AdafruitFeatherM0](https://badgen.net/badge/project/AdafruitFeatherM0/orange?icon=gitlab)](https://gitlab.com/bi_zeps/toolchains/-/blob/master/README.md#adafruit-feather-m0)
[![Stable Version](https://img.shields.io/docker/v/bizeps/AdafruitFeatherM0/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/bi_zeps/toolchains/-/blob/master/CHANGELOG.md#adafruit-feather-m0)
[![Docker Pulls](https://badgen.net/docker/pulls/bizeps/AdafruitFeatherM0?icon=docker&label=pulls)](https://hub.docker.com/r/bizeps/adafruitfeatherm0)
[![Docker Image Size](https://badgen.net/docker/size/bizeps/AdafruitFeatherM0/stable?icon=docker&label=size)](https://hub.docker.com/r/bizeps/adafruitfeatherm0)

# Adafruit Feather M0

Development in progress!

Image with arduino-cli installed to crosscompile for dedicated targets.

`arduino-cli compile -v --fqbn adafruit:samd:adafruit_feather_m0 --build-path ${BUILD_OUT_DIR} --log-file ${BUILD_OUT_DIR}/build.log ${PROJECT_SRC_DIR}/first_blink_sketch_sep27a`
